package org.uniglobalunion.spotlight.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.ActivityTransitionEvent;
import com.google.android.gms.location.ActivityTransitionResult;
import com.google.android.gms.location.DetectedActivity;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyRepository;

import java.util.ArrayList;
import java.util.Date;

import static org.uniglobalunion.spotlight.service.DetectedActivitiesTransitionReceiver.processActivityResultEvent;
import static org.uniglobalunion.spotlight.service.DetectedActivitiesTransitionReceiver.processTransitionResultEvent;

public class DetectedActivitiesIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DetectedActivitiesIntentService(String name) {
        super(name);
    }

    private final static String TAG = "DAIS";

    @Override
    public void onHandleIntent(Intent intent) {

        Log.d(TAG,"got activity: " + intent);

        if (ActivityTransitionResult.hasResult(intent)) {
            ActivityTransitionResult result = ActivityTransitionResult.extractResult(intent);

            if (result != null) {
                for (ActivityTransitionEvent event : result.getTransitionEvents())
                    processTransitionResultEvent(this, event);
            }
        }

        if (ActivityRecognitionResult.hasResult(intent)) {

//If data is available, then extract the ActivityRecognitionResult from the Intent//
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

//Get an array of DetectedActivity objects//
            ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();
            for (DetectedActivity event : detectedActivities)
                processActivityResultEvent(this, event);

        }

    }


}
