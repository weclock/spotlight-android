package org.uniglobalunion.spotlight

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.common.util.IOUtils
import com.maxkeppeler.sheets.info.InfoSheet
import com.maxkeppeler.sheets.input.InputSheet
import com.maxkeppeler.sheets.input.type.InputCheckBox
import com.maxkeppeler.sheets.input.type.InputEditText
import com.maxkeppeler.sheets.input.type.InputRadioButtons
import org.cleaninsights.sdk.*
import java.io.File
import java.io.IOException
import java.util.ArrayList

open class CleanInsightsManager {

    val CI_CAMPAIGN = "measurement-accuracy"

    private var mMeasure: CleanInsights? = null
    private var mHasConsent = true

    fun initMeasurement(context : Context) {
        if (mMeasure == null) {
            // Instantiate with configuration and directory to write store to, best in an `Application` subclass.
            try {

                mMeasure = CleanInsights(
                    context.assets.open("cleaninsights.json").reader().readText(),
                    context.filesDir
                )

                mHasConsent = mMeasure!!.isCampaignCurrentlyGranted(CI_CAMPAIGN)


            } catch (e: IOException) {
                e.printStackTrace()
            }
        }


    }


    private fun getConsent(context: Activity, campaignId: String?, viewId: String, isAccurate: Boolean) {

        mMeasure?.testServer {
            Log.d("Clean Insights", "test server: " + it.toString())
        }

        var success = mMeasure!!.requestConsent(campaignId!!, object : JavaConsentRequestUi {
            override fun show(
                s: String,
                campaign: Campaign,
                consentRequestUiCompletionHandler: ConsentRequestUiCompletionHandler
            ) {

                InfoSheet().show(context) {
                    title(context.getString(R.string.ci_title))
                    content(context.getString(R.string.clean_insight_consent_prompt))
                    onNegative(context.getString(R.string.ci_negative)) {
                        // Handle event
                        consentRequestUiCompletionHandler.completed(false)
                    }
                    onPositive(context.getString(R.string.ci_confirm)) {
                        // Handle event
                        mHasConsent = true
                        consentRequestUiCompletionHandler.completed(true)
                        mMeasure!!.grant(campaignId)

                        addMeasurementAccuracy(context, viewId, isAccurate)

                    }
                }

            }

            override fun show(
                feature: Feature,
                consentRequestUiCompletionHandler: ConsentRequestUiCompletionHandler
            ) {

            }


        })

        return success
    }

    fun measureView(view: String, campaignId: String?) {
        if (mHasConsent) {
            val alPath = ArrayList<String>()
            alPath.add(view)
            mMeasure?.measureVisit(alPath, campaignId!!)
        }
    }

    fun measureEvent(key: String?, value: String?, campaignId: String?) {
        if (mHasConsent) {
            mMeasure?.measureEvent(key!!, value!!, campaignId!!)

        }
    }

    fun persistMeasurements () {
        mMeasure?.persist()
    }

    /**
    fun showInfo (context : Activity) {


    }**/

    fun addMeasurementAccuracy (activity : Activity, viewId: String, isAccurate: Boolean) {

        if (!mHasConsent) {
            val result = getConsent(activity, CI_CAMPAIGN, viewId, isAccurate)

        }
        else {
            measureEvent("general_accuracy:$viewId", isAccurate.toString(), CI_CAMPAIGN)
        }

    }


    fun showConfirm (activity : Activity, viewId: String, isAccurate: Boolean) {

        if (!mHasConsent) {
            val result = getConsent(activity, CI_CAMPAIGN, viewId, isAccurate)

        }
        else {

            measureView("survey:" + viewId, CI_CAMPAIGN)

            if (isAccurate) {
                InfoSheet().show(activity) {
                    title(activity.getString(R.string.measurement_survey_title))
                    content(activity.getString(R.string.measurement_survey_content))
                    onNegative(activity.getString(R.string.cancel)) {

                        mMeasure?.persist()
                    }
                    onPositive(activity.getString(R.string.action_okay)) {
                        addMeasurementAccuracy(activity, viewId, isAccurate)
                        mMeasure?.persist()
                    }
                }
            }
            else
            {
                InfoSheet().show(activity) {
                    title(activity.getString(R.string.measurement_survey_title_fail))
                    content(activity.getString(R.string.measurement_survey_content_fail))
                    onNegative(activity.getString(R.string.cancel)) {

                        mMeasure?.persist()
                    }
                    onPositive(activity.getString(R.string.action_okay)) {
                        addMeasurementAccuracy(activity, viewId, isAccurate)
                        mMeasure?.persist()
                    }
                }
            }
        }
    }


}
