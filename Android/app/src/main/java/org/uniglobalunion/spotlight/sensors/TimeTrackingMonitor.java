package org.uniglobalunion.spotlight.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.service.TrackingService;

/**
 * Created by n8fr8 on 3/10/17.
 */
public class TimeTrackingMonitor {


    /**
     * Last update of the accelerometer
     */
    private long lastUpdate = -1;

    private TrackingService mContext;

    private long startTime = -1;
    private long endTime = -1;

    final Handler handler = new Handler();

    boolean keepRunning = false;

    public TimeTrackingMonitor(TrackingService context) {

        mContext = context;


        start ();

        Runnable runnable = new Runnable() {

            @Override
            public void run() {

                if (keepRunning) {
                    try {
                        endTime = System.currentTimeMillis();
                        mContext.logEvent(StudyEvent.EVENT_TYPE_TIME, getElapsedTime() + "");
                        startTime = System.currentTimeMillis();

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {

                        //also call the same runnable to call it at regular interval
                        handler.postDelayed(this, 60000);

                    }
                }
            }
        };
        handler.postDelayed(runnable, 60000);


    }

    public void start () {

        startTime = System.currentTimeMillis();
        mContext.logEvent(StudyEvent.EVENT_TYPE_TIME,"0");

        keepRunning = true;
    }

    public void stop(Context context) {

        endTime = System.currentTimeMillis();
        mContext.logEvent(StudyEvent.EVENT_TYPE_TIME,getElapsedTime()+"");

        keepRunning = false;
    }

    public long getElapsedTime ()
    {
        return endTime - startTime;
    }



}
