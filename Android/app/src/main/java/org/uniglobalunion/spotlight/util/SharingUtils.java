package org.uniglobalunion.spotlight.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.FileProvider;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.service.TrackingService;
import org.uniglobalunion.spotlight.ui.StudyResultsActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.applandeo.materialcalendarview.utils.DateUtils.getCalendar;

public class SharingUtils {

    // Method when launching drawable within Glide


    /**
     * Draw the view into a bitmap.
     */
    public static Bitmap loadBitmapFromView(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
          //  Log.e(TAG, "failed getViewBitmap(" + v + ")", new RuntimeException());
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        return bitmap;
    }

    public static void shareBitmapWithText (Context mContext, Bitmap bitmap, String shareString, boolean showDate)
    {
        String currDate = "";
        if (showDate)
            currDate = SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG).format(getCalendar().getTime());

        Bitmap shareBitmap = bitmap;

        if (!TextUtils.isEmpty(shareString))
            shareBitmap = SharingUtils.drawTextToBitmap(mContext,bitmap,currDate + " " + shareString);
        else if (showDate)
            shareBitmap = SharingUtils.drawTextToBitmap(mContext,bitmap,currDate);

        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, SharingUtils.getBitmapFromDrawable(mContext, shareBitmap));
        shareIntent.putExtra(Intent.EXTRA_TEXT,mContext.getString(R.string.share_insight_text));

        mContext.startActivity(Intent.createChooser(shareIntent, mContext.getString(R.string.share_image_using)));
    }

    public static Uri getBitmapFromDrawable(Context context, Bitmap bmp){



        // Store image to default external storage directory

        Uri bmpUri = null;

        try {

            // Use methods on Context to access package-specific directories on external storage.

            // This way, you don't need to request external read/write permission.

            // See https://youtu.be/5xVh-7ywKpE?t=25m25s

            File fileDir = new File(context.getFilesDir(), "images");
            File file =  new File(fileDir, context.getString(R.string.app_name) + "_map_exported_" + System.currentTimeMillis() + ".jpg");
            file.getParentFile().mkdirs();

            FileOutputStream out = new FileOutputStream(file);

            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);

            out.close();


            // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
            bmpUri = FileProvider.getUriForFile(context, TrackingService.FILE_AUTHORITY, file);


            // **Note:** For API < 24, you may use bmpUri = Uri.fromFile(file);



        } catch (IOException e) {

            e.printStackTrace();

        }

        return bmpUri;

    }

    public static Bitmap drawTextToBitmap(Context gContext,
                                  Bitmap bitmap,
                                   String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;


        int bHeight = bitmap.getHeight();
        int bWidth = bitmap.getWidth();

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);

        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.rgb(61, 61, 61));
        // text size in pixels
        paint.setTextSize((int) (14 * scale));
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width())/2;
        //int y = (bitmap.getHeight() + bounds.height())/2;
        int y = bounds.height()*2;

        Paint paintBg = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintBg.setColor(Color.WHITE);
        Rect boundsBg = new Rect(bounds.left+x-3,bounds.top+y-3,bounds.right+x+3,bounds.bottom+y+3);

        canvas.drawRect(boundsBg,paintBg);

        canvas.drawText(gText, x, y, paint);

        /**
        canvas.drawText("https://weclock.it", 3, bWidth-20, paint);

        Bitmap logo =  BitmapFactory.decodeResource(gContext.getResources(), R.mipmap.ic_launcher);
        canvas.drawBitmap(logo, 3, bHeight-40,paint);
         **/

        return bitmap;
    }
}
