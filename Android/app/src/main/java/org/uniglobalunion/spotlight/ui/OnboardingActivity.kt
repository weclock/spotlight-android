package org.uniglobalunion.spotlight.ui

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.github.appintro.AppIntro2
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.uniglobalunion.spotlight.R

class OnboardingActivity : AppIntro2() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = ""
        supportActionBar!!.hide()
        isWizardMode = true
        val title: String? = null
        val description: String? = null
        addSlide(
            CustomIntroSlide.newInstance(
                R.layout.slide_custom_bigtext,
                getString(R.string.ob1_title),
                getString(R.string.ob1_desc)
            )
        )
        addSlide(
            CustomIntroSlide.newInstance(
                R.layout.slide_custom_bigtext,
                getString(R.string.ob2_title),
                getString(R.string.ob2_desc)
            )
        )
        /**
         * title = getString(R.string.ob1_title);
         * description = getString(R.string.ob1_desc);
         * addSlide(AppIntroFragment.newInstance(title, null, description,  null, R.drawable.onboarding1, getResources().getColor(R.color.leku_white),getResources().getColor(R.color.app_text),getResources().getColor(R.color.app_text_light)));
         *
         * title = getString(R.string.ob2_title);
         * description = getString(R.string.ob2_desc);
         * addSlide(AppIntroFragment.newInstance(title, null, description,  null, R.drawable.onboarding2, getResources().getColor(R.color.leku_white),getResources().getColor(R.color.app_text),getResources().getColor(R.color.app_text_light)));
         */

        if (Build.VERSION.SDK_INT >= 33) {
            notificationPermissionLauncher.launch(android.Manifest.permission.POST_NOTIFICATIONS)
        } else {
            hasNotificationPermissionGranted = true
        }

    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        prefs.edit().putBoolean("onboarding", true).commit()

        // Do something when users tap on Done button.
        finish()
        startActivity(Intent(this, SurveyActivity::class.java))

        //startActivity(new Intent(this, LoadingActivity.class));
    }

    var hasNotificationPermissionGranted = false

    private val notificationPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            hasNotificationPermissionGranted = isGranted
            if (!isGranted) {
                if (Build.VERSION.SDK_INT >= 33) {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.POST_NOTIFICATIONS)) {
                        showNotificationPermissionRationale()
                    } else {
                        showSettingDialog()
                    }
                }
            } else {

            }
        }

    private fun showSettingDialog() {
        MaterialAlertDialogBuilder(this, com.google.android.material.R.style.MaterialAlertDialog_Material3)
            .setTitle(getString(R.string.notification_permission))
            .setMessage(getString(R.string.notification_permission_is_required_please_allow_notification_permission_from_setting))
            .setPositiveButton(R.string.action_okay) { _, _ ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                intent.data = Uri.parse("package:$packageName")
                startActivity(intent)
            }
            .setNegativeButton(R.string.ci_negative, null)
            .show()
    }

    private fun showNotificationPermissionRationale() {

        MaterialAlertDialogBuilder(this, com.google.android.material.R.style.MaterialAlertDialog_Material3)
            .setTitle(getString(R.string.alert))
            .setMessage(getString(R.string.notification_permission_is_required_to_show_notification))
            .setPositiveButton(R.string.action_okay) { _, _ ->
                if (Build.VERSION.SDK_INT >= 33) {
                    notificationPermissionLauncher.launch(android.Manifest.permission.POST_NOTIFICATIONS)
                }
            }
            .setNegativeButton(R.string.ci_negative, null)
            .show()
    }
}
