package org.uniglobalunion.spotlight.ui;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;


import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.applandeo.materialcalendarview.utils.EventDayUtils;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudyViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentStudyTotal#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentStudyTotal extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String studyId;
    private StudyListing mStudyListing;

    private ArrayList<String> mStudyIdFilter;

    private CalendarView calendarView;
    ArrayList<EventDay> events = new ArrayList<>();
    private Calendar mCurrentCal;

    public FragmentStudyTotal() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStudyTotal.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStudyTotal newInstance(String param1, String param2) {
        FragmentStudyTotal fragment = new FragmentStudyTotal();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        studyId = getActivity().getIntent().getStringExtra("id");
        mStudyListing = ((SpotlightApp)getActivity().getApplication()).getStudy(studyId);


        mStudyIdFilter = new ArrayList<>();

        if (!TextUtils.isEmpty(studyId)) {

            if (mStudyListing != null)
            {
                if (mStudyListing.trackActivities) {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_MOTION);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_SCREEN);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_DEVICE_POWER);


                }

                if (mStudyListing.trackEnviron)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_TEMPERATURE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_LIGHT);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_HUMIDITY);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_SOUND);
                }


                if (mStudyListing.trackGeo)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_GEO_FENCE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_COMMUTE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_GEO_LOGGING);
                }

                if (mStudyListing.trackEvent)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_REPORT);
                }

                if (mStudyListing.trackApps)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_SCREEN);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_APP_USAGE);

                }

                if (mStudyListing.trackTime)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_TIME);
                }

            }
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() != null)
                initCalendar();

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() != null)
            initCalendar();
    }

    private void initCalendar ()
    {
        if (mCurrentCal == null) {
            mCurrentCal = Calendar.getInstance();

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_study_total, container, false);
        initView (view);
        return view;
    }

    private void initView (View view)
    {


        calendarView = view.findViewById(R.id.calendarView);

       calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {


                ((StudyExplorerActivity)getActivity()).setCalendar(eventDay.getCalendar());

            }
        });

       calendarView.setOnPreviousPageChangeListener(new OnCalendarPageChangeListener() {
           @Override
           public void onChange() {
               initDates(calendarView.getCurrentPageDate());
           }
       });

        calendarView.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                initDates(calendarView.getCurrentPageDate());
            }
        });


        initDates(calendarView.getCurrentPageDate());

    }

    Date dateStart, dateEnd;

    @SuppressLint("StaticFieldLeak")
    private void initDates (Calendar cal)
    {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 1);
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        dateStart = cal.getTime();

        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        dateEnd = cal.getTime();


        StudyViewModel svm = new StudyViewModel(getActivity().getApplication());
        svm.getStudyEvents(mStudyIdFilter, dateStart.getTime(), dateEnd.getTime()).observe(getViewLifecycleOwner(), studyEvents -> {

            events.clear();

            long oneDayMs = 1000 * 60 * 60;

            long lastTimeStamp = Calendar.getInstance().getTimeInMillis();

            for (StudyEvent event : studyEvents) {

                if (event.getTimestamp() < (lastTimeStamp - oneDayMs)) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date(event.getTimestamp()));
                    EventDay eday = new EventDay(calendar, R.drawable.award, getResources().getColor(R.color.app_accent));
                    events.add(eday);
                    lastTimeStamp = event.getTimestamp();

                }

            }

            calendarView.setEvents(events);

        });




    }



}
