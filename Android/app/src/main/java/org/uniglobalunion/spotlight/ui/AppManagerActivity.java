package org.uniglobalunion.spotlight.ui;

/* Copyright (c) 2009, Nathan Freitas, Orbot / The Guardian Project - http://openideals.com/guardian */
/* See LICENSE for licensing information */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.uniglobalunion.spotlight.AppInfo;
import org.uniglobalunion.spotlight.R;

import static org.uniglobalunion.spotlight.Constants.PREFS_KEY_APP_ENABLED;

public class AppManagerActivity extends AppCompatActivity implements OnClickListener {

    private GridView listApps;
    private ListAdapter adapterApps;
    private ProgressBar progressBar;
    private final static String TAG = "Orbot";
    PackageManager pMgr = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pMgr = getPackageManager();

        this.setContentView(R.layout.layout_apps);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listApps = findViewById(R.id.applistview);
        progressBar = findViewById(R.id.progressBar);

        setTitle(getString(R.string.title_choose_work_apps));
    }

    /*
     * Create the UI Options Menu (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
    //    MenuInflater inflater = getMenuInflater();
      //  inflater.inflate(R.menu.orbot_apps, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        else if (false)
        {
            mApps = null;
            reloadApps();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        reloadApps();
    }

    private void reloadApps () {
        new AsyncTask<Void, Void, Void>() {
            protected void onPreExecute() {
                // Pre Code
                progressBar.setVisibility(View.VISIBLE);
            }
            protected Void doInBackground(Void... unused) {
                loadApps(mPrefs);
                return null;
            }
            protected void onPostExecute(Void unused) {
                listApps.setAdapter(adapterApps);
                progressBar.setVisibility(View.GONE);
            }
        }.execute();


    }

    SharedPreferences mPrefs = null;
    ArrayList<AppInfo> mApps = null;

    private void loadApps (SharedPreferences prefs)
    {

        if (mApps == null)
            mApps = getApps(getApplicationContext(), prefs);

        final LayoutInflater inflater = getLayoutInflater();

        adapterApps = new ArrayAdapter<AppInfo>(this, R.layout.layout_apps_item, R.id.itemtext,mApps) {

            public View getView(int position, View convertView, ViewGroup parent) {

                ListEntry entry = null;

                if (convertView == null)
                    convertView = inflater.inflate(R.layout.layout_apps_item, parent, false);
                else
                    entry = (ListEntry) convertView.getTag();

                if (entry == null) {
                    // Inflate a new view
                    entry = new ListEntry();
                    entry.icon = (ImageView) convertView.findViewById(R.id.itemicon);
                    entry.box = (CheckBox) convertView.findViewById(R.id.itemcheck);
                    entry.text = (TextView) convertView.findViewById(R.id.itemtext);
                    convertView.setTag(entry);
                }

                final AppInfo app = mApps.get(position);

                if (entry.icon != null) {

                    try {
                        entry.icon.setImageDrawable(pMgr.getApplicationIcon(app.getPackageName()));
                        entry.icon.setOnClickListener(AppManagerActivity.this);
                        entry.icon.setTag(entry.box);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                if (entry.text != null) {
                    entry.text.setText(app.getName());
                    entry.text.setOnClickListener(AppManagerActivity.this);
                    entry.text.setTag(entry.box);
                }

                if (entry.box != null) {
                    entry.box.setOnClickListener(AppManagerActivity.this);
                    entry.box.setChecked(app.isTorified());
                    entry.box.setTag(app);
                }

                return convertView;
            }
        };


    }

    private static class ListEntry {
        private CheckBox box;
        private TextView text;
        private ImageView icon;
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onStop()
     */
    @Override
    protected void onStop() {
        super.onStop();

    }


    public ArrayList<AppInfo> getApps (Context context, SharedPreferences prefs)
    {

        String tordAppString = prefs.getString(PREFS_KEY_APP_ENABLED, "");
        String[] tordApps;

        StringTokenizer st = new StringTokenizer(tordAppString,"|");
        tordApps = new String[st.countTokens()];
        int tordIdx = 0;
        while (st.hasMoreTokens())
        {
            tordApps[tordIdx++] = st.nextToken();
        }
        Arrays.sort(tordApps);

        List<ApplicationInfo> lAppInfo = pMgr.getInstalledApplications(0);

        Iterator<ApplicationInfo> itAppInfo = lAppInfo.iterator();

        ArrayList<AppInfo> apps = new ArrayList<AppInfo>();

        ApplicationInfo aInfo = null;

        AppInfo app = null;

        while (itAppInfo.hasNext())
        {
            aInfo = itAppInfo.next();

            if ((aInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0 && ((aInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) == 0)) {
                continue;
            }

            app = new AppInfo();



           try
            {
                app.setName(pMgr.getApplicationLabel(aInfo).toString());
            }
            catch (Exception e)
            {
                // no name
                continue; //we only show apps with names
            }

            app.setEnabled(aInfo.enabled);
            app.setUid(aInfo.uid);
            app.setUsername(pMgr.getNameForUid(app.getUid()));
            app.setProcname(aInfo.processName);
            app.setPackageName(aInfo.packageName);
            app.setUsesInternet(true);

            // check if this application is allowed
            if (Arrays.binarySearch(tordApps, app.getUsername()) >= 0) {
                app.setTorified(true);
            }
            else
            {
                app.setTorified(false);
            }

            apps.add(app);

        }

        Collections.sort(apps, (o1, o2) -> {
            /* Some apps start with lowercase letters and without the sorting being case
               insensitive they'd appear at the end of the grid of apps, a position where users
               would likely not expect to find them.
             */
            if (o1.isTorified() == o2.isTorified())
                return o1.getName().compareToIgnoreCase(o2.getName());
            else if (o1.isTorified())
                return -1;
            else
                return 1;
        });


        return apps;
    }


    public void saveAppSettings (Context context)
    {

        StringBuilder tordApps = new StringBuilder();
        Intent response = new Intent();

        for (AppInfo tApp:mApps)
        {
            if (tApp.isTorified())
            {
                tordApps.append(tApp.getPackageName());
                tordApps.append("|");

                response.putExtra(tApp.getPackageName(),true);
            }
        }

        Editor edit = mPrefs.edit();
        edit.putString(PREFS_KEY_APP_ENABLED, tordApps.toString());
        edit.commit();

        setResult(RESULT_OK,response);
    }


    /**
     * Called an application is check/unchecked
     */
    /**
     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
     final TorifiedApp app = (TorifiedApp) buttonView.getTag();
     if (app != null) {
     app.setTorified(isChecked);
     }

     saveAppSettings(this);

     }**/




    public void onClick(View v) {

        CheckBox cbox = null;

        if (v instanceof CheckBox)
            cbox = (CheckBox)v;
        else if (v.getTag() instanceof CheckBox)
            cbox = (CheckBox)v.getTag();

        if (cbox != null) {
            final AppInfo app = (AppInfo) cbox.getTag();
            if (app != null) {
                app.setTorified(!app.isTorified());
                cbox.setChecked(app.isTorified());
            }

            saveAppSettings(this);

            setResult(RESULT_OK);
        }
    }




}
