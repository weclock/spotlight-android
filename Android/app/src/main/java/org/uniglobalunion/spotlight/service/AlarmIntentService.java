package org.uniglobalunion.spotlight.service;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import static org.uniglobalunion.spotlight.service.TrackingService.KEY_INSIGHTS;

public class AlarmIntentService extends IntentService {

    public AlarmIntentService() {
        super("AlarmIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intentAlaram) {

        Intent i = new Intent(this, TrackingService.class);

        if (intentAlaram.hasExtra(KEY_INSIGHTS)&&
                intentAlaram.getBooleanExtra(KEY_INSIGHTS,false))
        {
            //it is the end of the day, time for insights!
            i.putExtra(KEY_INSIGHTS,true);
            i.setAction(KEY_INSIGHTS);
        }

        ContextCompat.startForegroundService(this,i);
    }
}
