package org.uniglobalunion.spotlight.sensors;

import static android.content.Context.POWER_SERVICE;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOFENCE_ENTER;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOFENCE_EXIT;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_HOME;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_WORK;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.PowerManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.location.LocationManagerCompat;

import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.util.Date;

public class GPSLocationMonitor extends LocationMonitor {


    private final static int UPDATE_INTERVAL = 10*1000;
    private final static int FASTEST_INTERVAL = 3000;
    private final static int MINIMUM_DISTANCE = 3;

    PowerManager.WakeLock wakeLock;
    private final static String WAKELOK_TAG = "Spotlight::MyWakelockTag";


    private Location locationHome;
    private Location locationWork;
    public static Date timeExitHome = null;
    public static Date timeExitWork = null;

    public static boolean atWork = false, atHome = false;
    private int mWaypointDistance = 100;

    private PendingIntent mGeofencePendingIntent;


    private TrackingService mContext;

    private LocationManager mLocationManager = null;

    private LocationListener mListener = new LocationListener() {
        @Override
        public void onLocationChanged(@NonNull Location location) {

            if (location != null)
                logLocation(mContext, location);

        }
    };

    public synchronized void startLocationUpdates(TrackingService context, boolean highResLoc) {

        if (mLocationManager != null)
            return;

        mContext = context;

        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
            return;
        }

        mLocationManager =
                (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


        if (highResLoc && gpsEnabled)
        {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, FASTEST_INTERVAL, MINIMUM_DISTANCE, mListener);


            /**
            locationRequest.setInterval(UPDATE_INTERVAL);
            locationRequest.setFastestInterval(FASTEST_INTERVAL);
            locationRequest.setSmallestDisplacement(MINIMUM_DISTANCE);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

             **/



            if (wakeLock == null || (!wakeLock.isHeld())) {
                PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);
                wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                        WAKELOK_TAG);
                wakeLock.acquire();
            }
        }
        else {

            mLocationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, FASTEST_INTERVAL, MINIMUM_DISTANCE, mListener);
        }

    }

    private void logLocation (TrackingService context, Location location)
    {
        String geoVal = location.getLatitude() + ","
                + location.getLongitude() + ","
                + location.getAccuracy() + ","
                + location.getSpeed() + ","
                + location.getAltitude() + ","
                + location.getBearing() + ","
                + location.getProvider();


        context.logEvent(StudyEvent.EVENT_TYPE_GEO_LOGGING,geoVal,location.getTime());

        if (locationHome != null && locationWork != null) {

            boolean nearHome = location.distanceTo(locationHome) < mWaypointDistance;
            boolean nearWork = location.distanceTo(locationWork) < mWaypointDistance;

            if (nearHome) {
                if (!atHome) {
                    context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_ENTER + ": home");

                    if (timeExitWork != null) {
                        long commuteTime = new Date().getTime()-timeExitWork.getTime();
                        context.logEvent(StudyEvent.EVENT_TYPE_COMMUTE, commuteTime + "");
                        timeExitWork = null;
                    }

                    atHome = true;

                }
            }
            else if (atHome)
            {
                context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_EXIT + ": home");
                atHome = false;
                timeExitHome = new Date();

            }


            if (nearWork) {

                if (!atWork) {
                    //entering work!
                    context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_ENTER + ": work");


                    if (timeExitHome != null) {
                        long commuteTime = new Date().getTime() - timeExitHome.getTime();
                        context.logEvent(StudyEvent.EVENT_TYPE_COMMUTE, commuteTime + "");
                        timeExitHome = null;
                    }

                    atWork = true;
                }

            }
            else if (atWork)
            {
                context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_EXIT + ": work");
                atWork = false;
                timeExitWork = new Date();
            }

        }
    }

    public void startGeofencing (TrackingService context, SharedPreferences mPrefs) {

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        String geoKey = GEOKEY_HOME;
        if (mPrefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {

            double geoLatitude = (double) mPrefs.getFloat("geo-" + geoKey + "-lat", -1);
            double geoLong = (double) mPrefs.getFloat("geo-" + geoKey + "-long", -1);

            locationHome = new Location("");
            locationHome.setLatitude(geoLatitude);
            locationHome.setLongitude(geoLong);

            doGeofencing(context, geoKey, geoLatitude, geoLong, mWaypointDistance);

        }

        geoKey = GEOKEY_WORK;
        if (mPrefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {
            double geoLatitude = (double) mPrefs.getFloat("geo-" + geoKey + "-lat", -1);
            double geoLong = (double) mPrefs.getFloat("geo-" + geoKey + "-long", -1);

            locationWork = new Location("");
            locationWork.setLatitude(geoLatitude);
            locationWork.setLongitude(geoLong);

            doGeofencing(context, geoKey, geoLatitude, geoLong, mWaypointDistance);

        }



    }

    public void doGeofencing(TrackingService context, String geofKey, double geoLat, double geoLong, float geoRad) {



    }

    public void stopGeofencing (TrackingService context)
    {


    }




    public void stopLocationTracking ()
    {

        if (mLocationManager != null)
            mLocationManager.removeUpdates(mListener);

        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
            wakeLock = null;
        }
    }



}
