package org.uniglobalunion.spotlight.security;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.beautycoder.pflockscreen.PFFLockScreenConfiguration;
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment;
import com.beautycoder.pflockscreen.security.PFResult;
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.ui.LoadingActivity;
import org.uniglobalunion.spotlight.ui.StudyPickerActivity;

import java.util.concurrent.locks.Lock;

public class LockScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen);
        showLockScreenFragment();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //PFSecurityManager.getInstance().setPinCodeHelper(new TestPFPinCodeHelperImpl());
    }

    private final PFLockScreenFragment.OnPFLockScreenCodeCreateListener mCodeCreateListener =
            new PFLockScreenFragment.OnPFLockScreenCodeCreateListener() {
        @Override
        public void onCodeCreated(String encodedCode) {
            PreferencesSettings.saveToPref(LockScreenActivity.this, "code",encodedCode);
            PreferencesSettings.saveToPref(LockScreenActivity.this,"locked",true);
            finish();
        }

                @Override
                public void onNewCodeValidationFailed() {

                }
            };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private final PFLockScreenFragment.OnPFLockScreenLoginListener mLoginListener =
            new PFLockScreenFragment.OnPFLockScreenLoginListener() {

        @Override
        public void onCodeInputSuccessful() {
            showMainFragment();
        }

        @Override
        public void onFingerprintSuccessful() {
            showMainFragment();
        }

        @Override
        public void onPinLoginFailed() {
            Toast.makeText(LockScreenActivity.this, "Pin failed", Toast.LENGTH_SHORT).show();
            PreferencesSettings.saveToPref(LockScreenActivity.this,"locked",true);

        }

        @Override
        public void onFingerprintLoginFailed() {
            Toast.makeText(LockScreenActivity.this, "Fingerprint failed", Toast.LENGTH_SHORT).show();
            PreferencesSettings.saveToPref(LockScreenActivity.this,"locked",true);

        }
    };

    private void showLockScreenFragment() {

        new PFPinCodeViewModel().isPinCodeEncryptionKeyExist().observe(
                this,
                new Observer<PFResult<Boolean>>() {
                    @Override
                    public void onChanged(@Nullable PFResult<Boolean> result) {
                        if (result == null) {
                            return;
                        }
                        if (result.getError() != null) {
                            Toast.makeText(LockScreenActivity.this, "Can not get pin code info", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        showLockScreenFragment(result.getResult());
                    }
                }
        );
    }

    private void showLockScreenFragment(boolean isPinExist) {
        final PFFLockScreenConfiguration.Builder builder = new PFFLockScreenConfiguration.Builder(this)
                .setTitle(isPinExist ? "Unlock with your pin code or fingerprint" : "Create Code")
                .setCodeLength(6)
                .setUseFingerprint(true);
        final PFLockScreenFragment fragment = new PFLockScreenFragment();

        /**
        fragment.setOnLeftButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LockScreenActivity.this, "Left button pressed", Toast.LENGTH_LONG).show();
            }
        });**/

        builder.setMode(isPinExist
                ? PFFLockScreenConfiguration.MODE_AUTH
                : PFFLockScreenConfiguration.MODE_CREATE);
        if (isPinExist) {
            fragment.setEncodedPinCode(PreferencesSettings.getPref(this,"code"));
            fragment.setLoginListener(mLoginListener);
        }

        fragment.setConfiguration(builder.build());
        fragment.setCodeCreateListener(mCodeCreateListener);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_view, fragment).commit();

    }

    private  void showMainFragment() {
        PreferencesSettings.saveToPref(LockScreenActivity.this,"locked",false);
        startActivity(new Intent(this, LoadingActivity.class));
        finish();
    }



}
