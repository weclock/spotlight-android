package org.uniglobalunion.spotlight.sensors;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;

import android.Manifest;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.model.StudyListing;

public class PermissionManager {


    public static void enableUsageTracking (Activity activity) {

        if (!checkForAppsPermission(activity)) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            activity.startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(activity.getString(R.string.prompt_app_usage_permission)).setPositiveButton(R.string.ok_pf, dialogClickListener)
                    .setNegativeButton(R.string.cancel_pf, dialogClickListener).show();

        }

    }

    private static boolean checkForAppsPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    public static void checkLocationPermissions(Activity activity, int request, boolean showDialog) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (showDialog) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                                        request);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage(activity.getString(R.string.location_permission_disclosure))
                        .setPositiveButton(R.string.location_permission_disclosure_ok, dialogClickListener)
                        .setNegativeButton(R.string.not_now, dialogClickListener).show();
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        request);
            }
        }
    }


    public static void checkLocationPermissions (Activity activity, int request)
    {
        checkLocationPermissions(activity, request, true);
    }

    public static boolean checkStepPermission (Activity activity, int request)
    {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACTIVITY_RECOGNITION},
                    request);

            return false;
        }
        else
            return true;
    }

    public static boolean checkNotificationPermission (Activity activity, int request)
    {
        if (Build.VERSION.SDK_INT >= 33) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.POST_NOTIFICATIONS},
                        request);

                return false;
            } else
                return true;
        }
        else
            return true;
    }

    public static  boolean checkEnvironPermissions (Activity activity, int request)
    {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO},
                    request);

            return false;
        }
        else
            return true;
    }

    public static boolean checkStudyPermission(Activity activity, StudyListing studyListing) {
        if (studyListing.trackApps)
            return checkForAppsPermission(activity);
        else if (studyListing.trackGeo)
            return ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        else if (studyListing.trackActivities)
            return ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_GRANTED;
        return true;
    }
}
