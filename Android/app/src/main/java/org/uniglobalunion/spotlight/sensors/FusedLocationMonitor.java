package org.uniglobalunion.spotlight.sensors;

import static android.app.PendingIntent.FLAG_IMMUTABLE;
import static android.content.Context.POWER_SERVICE;
import static org.uniglobalunion.spotlight.Constants.PREF_HIGHRES_LOCATION;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOFENCE_ENTER;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOFENCE_EXIT;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_HOME;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_WORK;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.util.ArrayList;
import java.util.Date;

public class FusedLocationMonitor extends LocationMonitor {


    private GeofencingClient mGeofClient;
    private ArrayList<Geofence> mGeoList;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback mLocationCallback;
    private final static int UPDATE_INTERVAL = 10*1000;
    private final static int FASTEST_INTERVAL = 3000;
    private final static int MINIMUM_DISTANCE = 3;

    PowerManager.WakeLock wakeLock;
    private final static String WAKELOK_TAG = "Spotlight::MyWakelockTag";


    private Location locationHome;
    private Location locationWork;
    public static Date timeExitHome = null;
    public static Date timeExitWork = null;

    public static boolean atWork = false, atHome = false;
    private int mWaypointDistance = 100;

    private PendingIntent mGeofencePendingIntent;



    public synchronized void startLocationUpdates(TrackingService context, boolean highResLoc) {


        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mFusedLocationClient != null && locationRequest != null)
            return; //already setup

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

        locationRequest = new LocationRequest();

        if (highResLoc)
        {

            locationRequest.setInterval(UPDATE_INTERVAL);
            locationRequest.setFastestInterval(FASTEST_INTERVAL);
            locationRequest.setSmallestDisplacement(MINIMUM_DISTANCE);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            if (wakeLock == null || (!wakeLock.isHeld())) {
                PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);
                wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                        WAKELOK_TAG);
                wakeLock.acquire();
            }
        }
        else {
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        mLocationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult == null) {
                    return;
                }

                for (Location location : locationResult.getLocations()) {

                    String geoVal = location.getLatitude() + ","
                            + location.getLongitude() + ","
                            + location.getAccuracy() + ","
                            + location.getSpeed() + ","
                            + location.getAltitude() + ","
                            + location.getBearing() + ","
                            + location.getProvider();


                    context.logEvent(StudyEvent.EVENT_TYPE_GEO_LOGGING,geoVal,location.getTime());

                    if (locationHome != null && locationWork != null) {

                        boolean nearHome = location.distanceTo(locationHome) < mWaypointDistance;
                        boolean nearWork = location.distanceTo(locationWork) < mWaypointDistance;

                        if (nearHome) {
                            if (!atHome) {
                                context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_ENTER + ": home");

                                if (timeExitWork != null) {
                                    long commuteTime = new Date().getTime()-timeExitWork.getTime();
                                    context.logEvent(StudyEvent.EVENT_TYPE_COMMUTE, commuteTime + "");
                                    timeExitWork = null;
                                }

                                atHome = true;

                            }
                        }
                        else if (atHome)
                        {
                            context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_EXIT + ": home");
                            atHome = false;
                            timeExitHome = new Date();

                        }


                        if (nearWork) {

                            if (!atWork) {
                                //entering work!
                                context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_ENTER + ": work");


                                if (timeExitHome != null) {
                                    long commuteTime = new Date().getTime() - timeExitHome.getTime();
                                    context.logEvent(StudyEvent.EVENT_TYPE_COMMUTE, commuteTime + "");
                                    timeExitHome = null;
                                }

                                atWork = true;
                            }

                        }
                        else if (atWork)
                        {
                            context.logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_EXIT + ": work");
                            atWork = false;
                            timeExitWork = new Date();
                        }

                    }

                }

            }
        };

        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);


    }

    public void startGeofencing (TrackingService context, SharedPreferences mPrefs) {

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        String geoKey = GEOKEY_HOME;
        if (mPrefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {

            double geoLatitude = (double) mPrefs.getFloat("geo-" + geoKey + "-lat", -1);
            double geoLong = (double) mPrefs.getFloat("geo-" + geoKey + "-long", -1);

            locationHome = new Location("");
            locationHome.setLatitude(geoLatitude);
            locationHome.setLongitude(geoLong);

            doGeofencing(context, geoKey, geoLatitude, geoLong, mWaypointDistance);

        }

        geoKey = GEOKEY_WORK;
        if (mPrefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {
            double geoLatitude = (double) mPrefs.getFloat("geo-" + geoKey + "-lat", -1);
            double geoLong = (double) mPrefs.getFloat("geo-" + geoKey + "-long", -1);

            locationWork = new Location("");
            locationWork.setLatitude(geoLatitude);
            locationWork.setLongitude(geoLong);

            doGeofencing(context, geoKey, geoLatitude, geoLong, mWaypointDistance);

        }



    }

    public void doGeofencing(TrackingService context, String geofKey, double geoLat, double geoLong, float geoRad) {

        if (mGeofClient == null) {
            mGeofClient = LocationServices.getGeofencingClient(context);
            mGeoList = new ArrayList<>();
        }

        mGeoList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(geofKey)

                .setCircularRegion(
                        geoLat,
                        geoLong,
                        geoRad
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT | Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(1 * 60 * 1000) //1 minute for DWELL
                .build());

        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL);
        builder.addGeofences(mGeoList);
        builder.build();

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        mGeofClient.addGeofences(builder.build(), getGeofencePendingIntent(context))
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences added
                        // ...
                        Log.d("Location","geofence added");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to add geofences
                        Log.d("Location","geofence fail");

                    }
                });

    }

    public void stopGeofencing (TrackingService context)
    {
        if (mGeofClient != null)
            mGeofClient.removeGeofences(getGeofencePendingIntent(context))
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            // Geofences removed
                            // ...
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Failed to remove geofences
                            // ...
                        }
                    });

    }


    private PendingIntent getGeofencePendingIntent(TrackingService context) {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(context, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        int flags = PendingIntent.FLAG_UPDATE_CURRENT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            flags = PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE;
        }

        mGeofencePendingIntent = PendingIntent.getService(context, 123, intent, flags);
        return mGeofencePendingIntent;
    }


    public void stopLocationTracking ()
    {
        if (mFusedLocationClient != null && mLocationCallback != null)
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);

        mFusedLocationClient = null;
        mLocationCallback = null;
        locationRequest = null;

        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
            wakeLock = null;
        }
    }



}
