package org.uniglobalunion.spotlight.service;

import android.content.Context;
import android.content.Intent;

import androidx.legacy.content.WakefulBroadcastReceiver;

import org.uniglobalunion.spotlight.security.PreferencesSettings;

public class MyWakefulReceiver extends WakefulBroadcastReceiver {

    @Override


    public void onReceive(Context context, Intent intent) {

        if (PreferencesSettings.getPrefBool(context,"serviceRunning")) {

            // Start the service, keeping the device awake while the service is
            // launching. This is the Intent to deliver to the service.
            Intent service = new Intent(context, TrackingService.class);
            startWakefulService(context, service);
        }
    }
}
