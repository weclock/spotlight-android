package org.uniglobalunion.spotlight.service;

import static org.uniglobalunion.spotlight.Constants.PREF_LAST_UPLOAD;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;

import com.amplifyframework.AmplifyException;
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin;
import com.amplifyframework.core.Amplify;
import com.amplifyframework.logging.AndroidLoggingPlugin;
import com.amplifyframework.logging.LogLevel;
import com.amplifyframework.storage.s3.AWSS3StoragePlugin;
import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyRepository;
import org.uniglobalunion.spotlight.ui.StudyResultsActivity;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UploadWorker extends ListenableWorker {

    Context mContext;

    public UploadWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
        mContext = context;

        try {
            Amplify.addPlugin(new AndroidLoggingPlugin(LogLevel.VERBOSE));
            Amplify.addPlugin(new AWSCognitoAuthPlugin());
            Amplify.addPlugin(new AWSS3StoragePlugin());
            Amplify.configure(mContext);
            Log.i("Amplify", "Initialized Amplify");
        } catch (AmplifyException error) {
            Log.e("Amplify", "Could not initialize Amplify", error);
        }
    }

    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        // Get events up until right now
        Calendar cal = Calendar.getInstance();
        long endTime = cal.getTimeInMillis();

        // Get events starting from last recorded upload or from up to one day ago
        if (prefs.contains(PREF_LAST_UPLOAD)) {
            cal.setTimeInMillis(prefs.getLong(PREF_LAST_UPLOAD, 0));
        } else {
            cal.add(Calendar.DATE, -1);
        }
        long startTime = cal.getTimeInMillis();
        prefs.edit().putLong(PREF_LAST_UPLOAD, endTime).apply();

        StudyRepository repo = new StudyRepository(mContext);
        ListenableFuture<List<StudyEvent>> getStudyEvents = repo.getStudyEventsFuture(startTime, endTime);

        Function<List<StudyEvent>, Result> sendStudyEvents = studyEvents -> {
            String fileName = StudyResultsActivity.getReportFileName(mContext);
            StudyResultsActivity.getAndSendReport(mContext, fileName, studyEvents);
            return Result.success();
        };
        Executor listeningExecutor = Executors.newSingleThreadExecutor();

        return Futures.transform(getStudyEvents, sendStudyEvents, listeningExecutor);
    }
}
